import { Injectable } from "@angular/core";
import {HttpEvent,HttpHandler,HttpInterceptor,HttpRequest} from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
    providedIn:'root'
})

export class AppInterceptor implements HttpInterceptor{

    constructor(){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        if(!req.headers.has('Authorization')){
            return next.handle(req.clone({
                setHeaders: {
                    Authorization: `${localStorage.getItem('token') || ''}`,
                  },
            
            }));
            
        }
        return next.handle(req);
        
    }

}

