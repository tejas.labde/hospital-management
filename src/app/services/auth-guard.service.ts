import { AuthorizationService } from "../modules/authorization/authorization.services";
import { Injectable } from "@angular/core";
import { CanLoad, Route, UrlSegment, UrlTree } from "@angular/router";
import { Router } from "@angular/router";
import { Observable } from "rxjs";

@Injectable()

export class AuthGuardService implements CanLoad {
    constructor(private router: Router, private authService: AuthorizationService) { }

    roles=[
        {name:'admin'},
        {name:'doctor'},
        {name:'nurse'}
    ]
    canLoad(route: Route): boolean {
        let role=localStorage.getItem('role');
        for(let element of this.roles){
            if(role!=element.name){
                alert('You are not authorized to access this page')
                return false;
            }
        }
        return true;
    }
}

