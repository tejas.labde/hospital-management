// import { AuthGuardService } from './auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
    path:'',loadChildren:()=>import('./modules/authorization/authorization.module')
    .then(m=>m.AuthorizationModule)
  },
  {
    path:'admin',
    loadChildren:()=>import('./modules/admin/admin.module')
    .then(m=>m.AdminModule)
  },
  {
    path:'doctor',
    loadChildren:()=>import('./modules/doctors/doctors.module')
    .then(m=>m.DoctorsModule)
    ,canLoad:[AuthGuardService]
  },
  {
    path:'nurse',
    loadChildren:()=>import('./modules/nurses/nurses.module')
    .then(m=>m.NursesModule)
    ,canLoad:[AuthGuardService] 
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
