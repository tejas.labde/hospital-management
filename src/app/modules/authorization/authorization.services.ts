import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ILoginData } from './login.interface';

@Injectable({
    providedIn:'root'
})

export class AuthorizationService{

    constructor(private httpClient:HttpClient){}

    loginUser(loginData:ILoginData){
        return this.httpClient.post('http://localhost:3000/user/login',{
            email:loginData.email,
            password:loginData.password
        })
    }
}

