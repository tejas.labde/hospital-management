import {  MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { AuthorizationRouting } from './authorization.routing';
import { NgModule } from "@angular/core";
import {MatInputModule} from "@angular/material/input";
import { LoginPageComponent } from './components/login-page/login-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
    declarations:[
    LoginPageComponent
  ],
    imports:[
        AuthorizationRouting,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        MatButtonModule,
        ReactiveFormsModule
    ],
    bootstrap:[LoginPageComponent],
    providers:[],
    exports:[]
})

export class AuthorizationModule{};