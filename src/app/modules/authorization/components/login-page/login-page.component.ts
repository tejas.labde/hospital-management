import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup ,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../authorization.services';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  loginForm:FormGroup=this.fb.group({
    email:new FormControl('',[Validators.required,Validators.email]),
    password:new FormControl('',[Validators.required,Validators.minLength(8)])
  });
  constructor(private fb:FormBuilder,private router:Router,private authService:AuthorizationService) { }

  ngOnInit(): void {
  }

  usersRoles=[{role:'62cd75655853a5d2c643dbaa',name:'admin'},
          {role:'62cd75655853a5d2c643dbab',name:'doctor'},
          {role:'62cd75655853a5d2c643dbac',name:'nurse'}
]
  
  onSubmit(loginData:FormGroup){
    console.log('login data value',loginData.value);
    this.authService.loginUser(loginData.value).subscribe((response:any)=>{
      
      localStorage.setItem('id',response.data.userId);
      localStorage.setItem('token',response.data.token);
      localStorage.setItem('user',response.data.name);

      this.usersRoles.map(userRole=>{
        if(response.data.role===userRole.role){ 
          localStorage.setItem('role',userRole.name)
          this.router.navigate([userRole.name])
        }
      })
    })
    
  }

}
