import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import {MatDialogModule} from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { EditDialogBoxComponent } from './components/edit-dialog-box/edit-dialog-box.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { AddNurseComponent } from './components/add-nurse/add-nurse.component';
import { ChangeRequestComponent } from './components/change-request/change-request.component';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';


@NgModule({
    declarations:[
    EditDialogBoxComponent,
    AddUserComponent,
    AddNurseComponent,
    ChangeRequestComponent,
    AlertDialogComponent
  ],
    imports:[   
        HttpClientModule,
        CommonModule,
        MatDialogModule,
        MatSelectModule,
        ReactiveFormsModule,
        FormsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
    ],
    providers:[],
    exports:[],
    bootstrap:[]
})

export class SharedModule{};