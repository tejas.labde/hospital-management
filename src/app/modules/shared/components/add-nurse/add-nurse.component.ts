import { FormBuilder,FormGroup,FormControl } from '@angular/forms';
import { AdminService } from './../../../admin/admin.services';
import { Component, OnInit } from '@angular/core';
import { IDoctorData } from 'src/app/modules/admin/admin-interface';

@Component({
  selector: 'app-add-nurse',
  templateUrl: './add-nurse.component.html',
  styleUrls: ['./add-nurse.component.scss']
})
export class AddNurseComponent implements OnInit {

  constructor(private adminService:AdminService,private fb:FormBuilder) { }

 addNurseForm:FormGroup=this.fb.group({
  name:new FormControl(''),
  email:new FormControl(''),
  role:new FormControl('62cd75655853a5d2c643dbac'),
  assignedDoctor:new FormControl(''||null)
 })
  doctorsArray:IDoctorData[]=[]
  ngOnInit(): void {
    this.adminService.getAdminData().subscribe((response:any)=>{
      console.log(response);
      response.data.forEach((user:any)=>{
        if(user.role==='62cd75655853a5d2c643dbab'){this.doctorsArray.push(user);}
    })  
      console.log('nurses array',this.doctorsArray)
    })
  }

 onDialogSubmit(addNurseForm:any){
  console.log('sending this form',addNurseForm.value);
  //send data to the backend and push it to the array of the user where it has to be appended
  this.adminService.addNewNurse(addNurseForm.value).subscribe(resp=>{
    alert('user added')},
    error=>{alert('error while adding user')
    console.log(error)}
    
  )
 }  
}
