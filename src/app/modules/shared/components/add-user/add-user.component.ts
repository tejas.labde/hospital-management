import { Router } from '@angular/router';
import { AdminService } from './../../../admin/admin.services';
import { FormBuilder ,FormGroup,FormControl} from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  constructor(private fb:FormBuilder,private adminService:AdminService,private router:Router) { }

  nurseArray:any=[];
  addUserForm:FormGroup=this.fb.group({
    name:new FormControl(''),
    email:new FormControl(''),
    speciality:new FormControl(''),
    role:new FormControl('62cd75655853a5d2c643dbab'),
    nurses:new FormControl([])
  })

  ngOnInit(): void {
    this.adminService.getAdminData().subscribe((response:any)=>{
      console.log(response);
      response.data.forEach((user:any)=>{
        if(user.role==='62cd75655853a5d2c643dbac'){this.nurseArray.push(user);}
    })  
      console.log('nurses array',this.nurseArray)
    })
  }

  addUserSubmit(formValue:any){
    this.adminService.addUser(formValue.value).subscribe(resp=>{
      alert('user added')
      this.router.navigate(['/admin'])},
      error=>{alert('error while adding user')
      console.log(error)}
      
    )
  }
}
