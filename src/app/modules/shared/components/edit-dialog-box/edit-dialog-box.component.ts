import { Router } from '@angular/router';
import { AlertDialogComponent } from './../alert-dialog/alert-dialog.component';
import { AdminService } from './../../../admin/admin.services';
import { MatDialog,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup ,FormControl} from '@angular/forms';
import { Component, Input, OnInit,Inject } from '@angular/core';
import { IDoctorData,INurseData } from 'src/app/modules/admin/admin-interface';

@Component({
  selector: 'app-edit-dialog-box',
  templateUrl: './edit-dialog-box.component.html',
  styleUrls: ['./edit-dialog-box.component.scss']
})
export class EditDialogBoxComponent implements OnInit {

  constructor(public fb:FormBuilder,private adminService:AdminService,
    @Inject(MAT_DIALOG_DATA) public data: any,private dialog:MatDialog) { }

  @Input() name:string='';
    availableDoctors:IDoctorData[]=[]
    availableNurses:INurseData[]=[];
    

  ngOnInit(): void { console.log('data',this.data);
    this.getAdminData();
  }

  editForm:FormGroup=this.fb.group({
    name:new FormControl(this.data.name),
    email:new FormControl(this.data.email),
    speciality:new FormControl(this.data.speciality),
    nurses:new FormControl([]),
    id:new FormControl(this.data._id)
  })

  getAdminData(){
    this.adminService.getAdminData().subscribe((response:any)=>{
      console.log(response);
      response.data.forEach((user:any)=>{
        if(user.role==='62cd75655853a5d2c643dbab'){this.availableDoctors.push(user)}
        else if(user.role==='62cd75655853a5d2c643dbac'){this.availableNurses.push(user)}
    })  
    })
  }

  onEditDialogSubmit(editForm:FormGroup){ 
    console.log('submitting edit dialog box');
    this.adminService.updateUser(editForm.value).subscribe((response:any)=>{
        this.dialog.open(AlertDialogComponent,{
          data:true
        })
        ,
        (error:any)=>{
          this.dialog.open(AlertDialogComponent,{
            data:false
          })
        }
      })
      
    }
  }
