import { DoctorsService } from './../../../doctors/doctors.services';
import { NurseService } from './../../../nurses/nurses.services';
import { AdminService } from './../../../admin/admin.services';
import { ControlContainer, FormBuilder,FormGroup,FormControl,Validators} from '@angular/forms';
import { INurseData } from 'src/app/modules/admin/admin-interface';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-change-request',
  templateUrl: './change-request.component.html',
  styleUrls: ['./change-request.component.scss']
})
export class ChangeRequestComponent implements OnInit {

  constructor(private fb:FormBuilder,
    @Inject(MAT_DIALOG_DATA)public data:INurseData,
    private nurseService:NurseService,private doctorService:DoctorsService) { }

    availableUsers:any;
    
    initialUser:string='';
    role:string=localStorage.getItem('role') || '';

    changeRequestForm:FormGroup=this.fb.group({
      for:new FormControl(this.data._id),
      replacement:new FormControl('',[Validators.required]),
      reason:new FormControl('',[Validators.required])
    })

  ngOnInit(): void {

    if(this.role==='doctor'){
       this.doctorService.getAvailableNurses('62cd75655853a5d2c643dbac').subscribe((response:any)=>{
        this.availableUsers=response.data.filter((user:any)=>(user.assignedDoctor._id) != this.initialUser)

      })
    }
    if(this.role==='nurse'){
      
      this.initialUser=Object(this.data.assignedDoctor)._id;
      this.nurseService.getAvailableDoctors('62cd75655853a5d2c643dbab').subscribe((response:any)=>{
        
        this.availableUsers=response.data.filter((user:any)=>(user.assignedDoctor._id) != this.initialUser)
  
      })
    }
   
  }

  changeRequestSubmit(changeRequest:any){
    console.log('change request',changeRequest.value);
    this.nurseService.sendChangeRequest(changeRequest.value).subscribe(response=>{
      console.log(response);
    })
  }

}
