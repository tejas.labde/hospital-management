import { DoctorsService } from './../doctors.services';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-doctor-messages',
  templateUrl: './doctor-messages.component.html',
  styleUrls: ['./doctor-messages.component.scss']
})
export class DoctorMessagesComponent implements OnInit {

  
  columnsToDisplay:string[]=['nurseName','messagesCol'];
 
  tableDataSource!:MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!:MatPaginator;
  
  
  constructor(private doctorService:DoctorsService) { }

  ngOnInit(): void {
      this.doctorService.getDoctorsMessages().subscribe((response:any)=>{
          this.tableDataSource = new MatTableDataSource(response.data)
          this.tableDataSource.paginator = this.paginator;
          console.log(this.tableDataSource);
        })
      
  }

}
