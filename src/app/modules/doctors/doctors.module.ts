import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { DoctorsRouting } from './doctors.routing';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { DoctorUserComponent } from './doctor-user/doctor-user.component';
import { DoctorChangeRequestComponent } from './doctor-change-request/doctor-change-request.component';
import { DoctorMessagesComponent } from './doctor-messages/doctor-messages.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations:[
    DoctorUserComponent,
    DoctorChangeRequestComponent,
    DoctorMessagesComponent
  ],
    imports:[
        DoctorsRouting,
        CommonModule,
        MatPaginatorModule,
        MatTableModule,
        HttpClientModule,
        MatButtonModule,
        ReactiveFormsModule
    ],
    bootstrap:[],
    providers:[],
    exports:[]
})

export class DoctorsModule{};