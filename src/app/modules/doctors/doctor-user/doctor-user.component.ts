import { DoctorsService } from './../doctors.services';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-user',
  templateUrl: './doctor-user.component.html',
  styleUrls: ['./doctor-user.component.scss']
})
export class DoctorUserComponent implements OnInit {

  constructor(private router:Router,private doctorService:DoctorsService) { }

  ngOnInit(): void {
    this.doctorService.getDoctorProfile().subscribe(response=>{
      console.log(response);
    })
  }

  changeReq:boolean=true;messages:boolean=false;

  onLogout(){
    localStorage.clear();
    this.router.navigate(['']);
  }

  onChangeReqClick(){
    this.changeReq=true;this.messages=false;
  }
  onMessagesClick(){
    this.messages=true;this.changeReq=false;
  }

}
