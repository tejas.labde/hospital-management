import { DoctorUserComponent } from './doctor-user/doctor-user.component';
import { NgModule } from "@angular/core";
import { RouterModule,Routes } from "@angular/router";

const routes:Routes=[{
    path:'',component:DoctorUserComponent
}]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class DoctorsRouting{};