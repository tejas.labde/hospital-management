import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn:'root'
})

export class DoctorsService{

    constructor(private httpClient:HttpClient){}

    getDoctorsMessages(){
        return this.httpClient.get(`http://localhost:3000/message`)
    }

    getDoctorProfile(){
        
        return this.httpClient.get('http://localhost:3000/user/profile')
    }

    getAvailableNurses(role:string){
        return this.httpClient.get(`http://localhost:3000/user?role=${role}&page=1&itemsPerPage=10&occupied=false`)
    }

    sendReminder(id:string){
        return this.httpClient.post(`http://localhost:3000/reminder/${id}`,id)
    }

    

   

}

