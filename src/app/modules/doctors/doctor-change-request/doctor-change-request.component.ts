import { MatDialog } from '@angular/material/dialog';
import { ChangeRequestComponent } from './../../shared/components/change-request/change-request.component';
import { DoctorsService } from './../doctors.services';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-doctor-change-request',
  templateUrl: './doctor-change-request.component.html',
  styleUrls: ['./doctor-change-request.component.scss']
})
export class DoctorChangeRequestComponent implements OnInit {

  constructor(private doctorService:DoctorsService,public dialog:MatDialog) { }

  tableDataSource!:MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!:MatPaginator;

  ngOnInit(): void {
    this.doctorService.getDoctorProfile().subscribe((response:any)=>{
      console.log('get wala data',response);
      this.tableDataSource = new MatTableDataSource(response.data.nurses)
      this.tableDataSource.paginator = this.paginator;
      console.log(this.tableDataSource);
    })
  }

  columnsToDisplay:string[]=['nurseName','changeRequestBtnColumn','sendReminderBtnColumn']
  
  onChangeRequest(nurseData:any){
    this.dialog.open(ChangeRequestComponent,{
      data:nurseData
    })
  }

  sendReminder(){
    
    this.doctorService.sendReminder(localStorage.getItem('id') || '').subscribe(response=>{
      alert('reminder sent')
      console.log(response);
    },
    error=>{
      alert('error while sending reminder')
      console.log(error);
    })
  }
  

  
}
