import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorChangeRequestComponent } from './doctor-change-request.component';

describe('DoctorChangeRequestComponent', () => {
  let component: DoctorChangeRequestComponent;
  let fixture: ComponentFixture<DoctorChangeRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorChangeRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DoctorChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
