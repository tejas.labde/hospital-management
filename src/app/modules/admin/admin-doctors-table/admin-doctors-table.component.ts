import { Router } from '@angular/router';
import { AddUserComponent } from './../../shared/components/add-user/add-user.component';
import { EditDialogBoxComponent } from '../../shared/components/edit-dialog-box/edit-dialog-box.component';
import { IDoctorData } from './../admin-interface';
import { AdminService } from './../admin.services';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-admin-doctors-table',
  templateUrl: './admin-doctors-table.component.html',
  styleUrls: ['./admin-doctors-table.component.scss']
})
export class AdminDoctorsTableComponent implements OnInit {

  constructor(public dialog: MatDialog, private adminService: AdminService,private router:Router) { }
  tableDataSource!:MatTableDataSource<IDoctorData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  columnsToDisplay: string[] = ['name','speciality', 'editBtnColumn', 'deleteBtnColumn'];

  ngOnInit():void{ this.getAdminData();}
  

  openDialog(): void {
    const dialogRef =
      this.dialog.open(AddUserComponent);
    dialogRef.afterClosed().subscribe(resp => {
      console.log('dialog has closed successfully');
      this.getAdminData();
    })
  }

  getAdminData(){
    this.adminService.getAdminData().subscribe(response=>{
      this.tableDataSource=new MatTableDataSource(
        Object(response).data.filter( (user:IDoctorData) =>user.role==='62cd75655853a5d2c643dbab' )
      )
      this.tableDataSource.paginator = this.paginator;
      console.log(this.tableDataSource);
      })
  }

  onEditClick(element:any){
    this.dialog.open(EditDialogBoxComponent,{
      data:element
    }
    );
    
  }

  onDeleteClick(element:IDoctorData){
    this.adminService.deleteUser(element).subscribe(response=>{
      console.log(response);
      alert('user deleted');
      this.getAdminData();
    })
  }
}
