import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDoctorsTableComponent } from './admin-doctors-table.component';

describe('AdminDoctorsTableComponent', () => {
  let component: AdminDoctorsTableComponent;
  let fixture: ComponentFixture<AdminDoctorsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDoctorsTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminDoctorsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
