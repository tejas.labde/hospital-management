import { AdminService } from './../admin.services';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-admin-requests',
  templateUrl: './admin-requests.component.html',
  styleUrls: ['./admin-requests.component.scss']
})
export class AdminRequestsComponent implements OnInit {

  constructor(private adminService:AdminService) { }

  columnsToDisplay: string[] = ['requestName', 'approveBtnColumn', 'deleteBtnColumn'];
  
  tableDataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit():void{
      this.adminService.getAdminChangeRequests().subscribe((response:any)=>{
        console.log('admin requests data',response);
        this.tableDataSource=new MatTableDataSource(response.data);
        this.tableDataSource.paginator = this.paginator;
      })
  }

  onApproveClick(request:any){
    request.status='62d113da98d581334c980362';
    this.adminService.updateChangeRequest(request).subscribe(response=>
      {alert('request changed successfully')},
    error=>{alert('error while updating request')}
    )
  }


  onDeleteClick(request:any){
    request.status='62d113da98d581334c980361';
    this.adminService.updateChangeRequest(request).subscribe(response=>
      {alert('request changed successfully')},
    error=>{alert('error while updating request')}
    )
  }

}
