import { AdminService } from './../admin.services';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.scss']
})
export class AdminUserComponent implements OnInit {

  constructor(private router:Router,private adminService:AdminService) { }

  ngOnInit(): void {

    this.adminService.getAdminData().subscribe(response=>{
      console.log('admin data',response);
    })

  }

  doctorsDisplay:boolean=true;
  nurseDisplay:boolean=false;
  notificationDisplay:boolean=false;
  requestsDisplay:boolean=false;

  
}
