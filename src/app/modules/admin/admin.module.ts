import { SharedModule } from './../shared/shared.module';
import { AdminRouting } from './admin.routing';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { AdminDoctorsTableComponent } from './admin-doctors-table/admin-doctors-table.component';
import {MatTableModule} from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { AdminNursesTableComponent } from './admin-nurses-table/admin-nurses-table.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AdminRequestsComponent } from './admin-requests/admin-requests.component';
import { AdminNotificationsComponent } from './admin-notifications/admin-notifications.component';
import {MatDialogModule} from '@angular/material/dialog';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';

@NgModule({
    declarations:[
    AdminUserComponent,
    AdminDoctorsTableComponent,
    AdminNursesTableComponent,
    AdminRequestsComponent,
    AdminNotificationsComponent,
    AdminSidebarComponent,
  ],
    imports:[
        AdminRouting,
        CommonModule,
        MatTableModule,
        SharedModule,
        MatDialogModule,
        MatPaginatorModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    bootstrap:[AdminUserComponent],
    providers:[],
    exports:[]
})

export class AdminModule{};