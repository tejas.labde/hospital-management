import { IDoctorData } from './admin-interface';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn:'root'
})

export class AdminService{

    constructor(private httpClient:HttpClient){}

     getAdminData(){
        return this.httpClient.get('http://localhost:3000/user')
    }
    getNotifications(){
        return this.httpClient.get('http://localhost:3000/reminder?deleted=false&page=1&itemsPerPage=10')
    }

    getAdminChangeRequests(){
        return this.httpClient.get('http://localhost:3000/change-request')
    }

    addUser(newUser:IDoctorData){
        return this.httpClient.post('http://localhost:3000/user/register',newUser)
    }
    updateUser(userValue:any){
        return this.httpClient.patch(`http://localhost:3000/user/${userValue.id}`,userValue)
    }

    deleteUser(userToBeDeleted:any){
        return this.httpClient.delete(`http://localhost:3000/user/${userToBeDeleted._id}`)
    }

    addNewNurse(newNurse:any){
        console.log('aagaye',newNurse)
        return this.httpClient.post(`http://localhost:3000/user/register`,JSON.stringify(newNurse))
    }

    updateChangeRequest(request:any){
        return this.httpClient.patch(`http://localhost:3000/change-request/${request._id}`,
        {'status':request.status})
    }
}
