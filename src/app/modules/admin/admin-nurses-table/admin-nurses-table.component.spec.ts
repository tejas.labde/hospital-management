import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNursesTableComponent } from './admin-nurses-table.component';

describe('AdminNursesTableComponent', () => {
  let component: AdminNursesTableComponent;
  let fixture: ComponentFixture<AdminNursesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminNursesTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminNursesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
