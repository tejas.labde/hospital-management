import { Router } from '@angular/router';
import { EditDialogBoxComponent } from './../../shared/components/edit-dialog-box/edit-dialog-box.component';
import { AddNurseComponent } from './../../shared/components/add-nurse/add-nurse.component';
import { INurseData } from './../admin-interface';
import { AdminService } from './../admin.services';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-admin-nurses-table',
  templateUrl: './admin-nurses-table.component.html',
  styleUrls: ['./admin-nurses-table.component.scss']
})
export class AdminNursesTableComponent implements OnInit {

  constructor(private dialog: MatDialog, private adminService: AdminService,private router:Router) { }

  columnsToDisplay: string[] = ['nurseName', 'doctorsName', 'editBtnColumn', 'deleteBtnColumn'];


  tableDataSource!: MatTableDataSource<INurseData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;


  openDialog(): void {
    const dialogRef =
      this.dialog.open(AddNurseComponent);
    dialogRef.afterClosed().subscribe(resp => {
      console.log('dialog has closed successfully');
    })
  }

  ngOnInit(): void {
      this.getAdminData();
  }

  getAdminData(){
    this.adminService.getAdminData().subscribe(response => {
      this.tableDataSource = new MatTableDataSource(
        Object(response).data.filter((user: INurseData) => user.role === '62cd75655853a5d2c643dbac')
      )
      this.tableDataSource.paginator = this.paginator;
      console.log(this.tableDataSource);
    })
  }

  onEditClick(element:any){
    this.dialog.open(EditDialogBoxComponent,{
      data:element
    }
    );
  }

  onDeleteClick(element:INurseData){
    this.adminService.deleteUser(element).subscribe(response=>{
      console.log(response);
      alert('user deleted');
      this.getAdminData();
    })
  }
}





