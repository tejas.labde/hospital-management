import { AdminDoctorsTableComponent } from './admin-doctors-table/admin-doctors-table.component';
import { AdminRequestsComponent } from './admin-requests/admin-requests.component';
import { AdminNotificationsComponent } from './admin-notifications/admin-notifications.component';
import { AdminNursesTableComponent } from './admin-nurses-table/admin-nurses-table.component';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [{ path: '', component: AdminDoctorsTableComponent },
{ path: 'nurse', component: AdminNursesTableComponent },
{ path: 'notifications', component: AdminNotificationsComponent },
{ path: 'requests', component: AdminRequestsComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminRouting { };