export interface IDoctorData{
    name:string,
    role:string,
    email:string,
    password:string,
    speciality:string,
    nurses:[],
    _id:string
}

export interface INurseData{
    name:string,
    email:string,
    password:string,
    role:string,
    assignedDoctor:[],
    _id:string
}

