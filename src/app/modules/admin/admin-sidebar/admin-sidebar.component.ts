import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.scss']
})
export class AdminSidebarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  onLogout(){
    localStorage.clear();
    this.router.navigate(['']);
  }

  onDoctorClick(){this.router.navigate(['admin'])}
  onNurseClick(){this.router.navigate(['admin/nurse'])}
  onNotificationClick(){this.router.navigate(['admin/notifications'])}
  onRequestsClick(){this.router.navigate(['admin/requests'])}
}
