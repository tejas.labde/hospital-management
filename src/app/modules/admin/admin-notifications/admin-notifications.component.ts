import { AdminService } from './../admin.services';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-admin-notifications',
  templateUrl: './admin-notifications.component.html',
  styleUrls: ['./admin-notifications.component.scss']
})

export class AdminNotificationsComponent implements OnInit {

  constructor(private adminService:AdminService) { }

  columnsToDisplay:string[]=['createdAt','updatedAt'];
  
  tableDataSource!:MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!:MatPaginator;
  
  
  ngOnInit(): void {
    this.adminService.getNotifications().subscribe((response:any)=>{
      console.log(response.data);
      this.tableDataSource=new MatTableDataSource(response.data);
    })
    
  }

}

