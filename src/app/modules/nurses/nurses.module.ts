import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { NursesRouting } from './nurses.routing';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { NurseUserComponent } from './nurse-user/nurse-user.component';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';

@NgModule({
    declarations:[
    NurseUserComponent,
    MessageDialogComponent
  ],
    imports:[
        NursesRouting,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatDialogModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    bootstrap:[],
    providers:[],
    exports:[]
})

export class NursesModule{};