import { NurseUserComponent } from './nurse-user/nurse-user.component';
import { NgModule } from "@angular/core";
import { RouterModule,Routes } from "@angular/router";

const routes:Routes=[{
    path:'',component:NurseUserComponent
}]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class NursesRouting{};