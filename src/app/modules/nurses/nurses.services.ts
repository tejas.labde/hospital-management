import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn:'root'
})


export class NurseService{

    constructor(private httpClient:HttpClient){}
    authorizationToken=localStorage.getItem('token')||'';

    getNurseProfile(){
        return this.httpClient.get('http://localhost:3000/user/profile',{
            headers:{Authorization:this.authorizationToken}
        })
    }

    sendMsgToDoctor(nurseMsg:any){
        return this.httpClient.post('http://localhost:3000/message',nurseMsg)
    }

    getAvailableDoctors(role:string){
        return this.httpClient.get(`http://localhost:3000/user?role=${role}&page=1&itemsPerPage=10&occupied=false`)
    }

    sendChangeRequest(changeRequest:any){
        return this.httpClient.post(`http://localhost:3000/change-request`,changeRequest)
    }

}

