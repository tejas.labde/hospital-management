import { INurseData } from './../../admin/admin-interface';
import { NurseService } from './../nurses.services';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, } from '@angular/material/dialog';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit {

  constructor(private fb:FormBuilder,private nurseService:NurseService,
    @Inject(MAT_DIALOG_DATA) public data: INurseData) { }

  ngOnInit(): void {
    console.log('data being injected',this.data);
    console.log('assigned doctor',this.data.assignedDoctor);
  }

  messageForm:FormGroup=this.fb.group({
    to:new FormControl(Object(this.data.assignedDoctor)._id),
    text:new FormControl('',[Validators.required])
    
  })

  onMessageSubmit(messageToDoctor:any){
    console.log('sending message from the nurse to the doctor',messageToDoctor.value);
    this.nurseService.sendMsgToDoctor(messageToDoctor.value).subscribe(response=>{
      console.log(response);
      alert('message sent successfully')
    },
    error=>{
      console.log(error);
      alert('error while sending message')
    })
  }
}
