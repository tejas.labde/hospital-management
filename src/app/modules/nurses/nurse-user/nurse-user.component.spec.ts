import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseUserComponent } from './nurse-user.component';

describe('NurseUserComponent', () => {
  let component: NurseUserComponent;
  let fixture: ComponentFixture<NurseUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NurseUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
