import { ChangeRequestComponent } from './../../shared/components/change-request/change-request.component';
import { INurseData } from './../../admin/admin-interface';
import { NurseService } from './../nurses.services';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nurse-user',
  templateUrl: './nurse-user.component.html',
  styleUrls: ['./nurse-user.component.scss']
})
export class NurseUserComponent implements OnInit {
  constructor(private router:Router,private dialog:MatDialog,private nurseService:NurseService) { }

  onLogout(){localStorage.clear();this.router.navigate(['']);}

  nurseDetails:any;
  nurseData=[{doctorName:"doctor1"}]
  columnsToDisplay:string[]=['doctorName','changeRequestBtnCol','sendMessageBtnCol'];

  ngOnInit(): void {
    this.nurseService.getNurseProfile().subscribe((response:any)=>{
      this.nurseDetails=Object(response.data);
      
    });
  }

  openMsgDialog(){
    this.dialog.open(MessageDialogComponent,{
      data:this.nurseDetails
    })
  }

  onChangeReqClick(){
    this.dialog.open(ChangeRequestComponent,{
      data:this.nurseDetails
    })
  }

}
